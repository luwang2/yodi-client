import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import '../css/login.css';
import { toast } from 'react-toastify';
import axios from 'axios';

const URL = "http://localhost:8080/api/user/signin";

function Login({ onLogin }) {
  const navigate = useNavigate();
  const [formErrors, setFormErrors] = useState({});
  const [apiError, setApiError] = useState("");
  const handleSubmit = async (e) => {
    e.preventDefault();

    // Clear previous API errors
    setApiError("");

    const email = document.getElementById("email").value;
    const password = document.getElementById("password").value;

    const errors = {};

    const emailValue = email;
    if (emailValue === "") {
      errors.email = "This field should not be empty";
    } else if (!emailValue.match(/^[^\s@]+@[^\s@]+\.[^\s@]+$/)) {
      errors.email = "Enter a valid email address";
    }

    const passwordValue = password;
    if (passwordValue === "") {
      errors.password = "Password is required";
    } else if (passwordValue.length < 6) {
      errors.password = "Password should be at least 6 characters long";
    }

    if (Object.keys(errors).length > 0) {
      setFormErrors(errors);
      return;
    }

    try {
      const response = await axios.post(URL, { email, password });
      if (response.data && response.data.status === 'success') {
        const user = response.data.data.user;

        const token = response.data.token;

        if (user && token) {
          const isAdmin = user.isAdmin;
          const isLoggedIn = true;
          console.log(token, isAdmin)
          localStorage.setItem('token', token);
          localStorage.setItem('isAdmin', isAdmin);
          onLogin(isLoggedIn, isAdmin);
          // onLogin(isLoggedIn)


          toast.success("Login successful");
          //console.log(isAdmin, "hihi")
          // isAdmin(true);
          navigate(isAdmin ? '/admin/home' : '/home');
        } else {
          setApiError("Login failed. User information is missing.");
        }
      } else {
        setApiError("Invalid credentials. Please try again.");
      }
    } catch (error) {
      if (error.response && error.response.status === 401) {
        setApiError("Invalid credentials. Please try again.");
      } else if (error.response && error.response.status === 404) {
        setApiError("User not found. Please sign up first.");
      } else {
        setApiError("An error occurred. Please try again later.");
      }
    }
    setFormErrors(errors);
  };

  return (
    <div className='d-flex justify-content-center align-items-center mx-5 my-5'>
      <div className='bg-white border p-3 rounded' style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)', width: "150vh" }}>
        <div className='d-flex justify-content-center align-item-center'>
          <img className='click_able_logo' src='./Assets/logoBlue.png' alt='clickable-logo'></img>
        </div>
        <div className='row'>
          <div className='col-md-6 d-none d-md-block'>
            <img className='login_side_image mb-10' src='./Assets/side_image.jpg' alt='side_image'></img>
          </div>
          <div className='col-md-6 '>
            <form onSubmit={handleSubmit}>
              <h3 className='text-center'>Welcome to <img className='logo' src='./Assets/logoBlue.png' alt='Yodi'></img>  Water Park</h3>
              <div className='mb-3'>
                <label className='mb-2 text_font' htmlFor='email'>User name or email address</label>
                <input
                  type="email"
                  id="email"
                  placeholder='example@gmail.com'
                  className={`form-control ${formErrors.email && 'is-invalid'} rounded-12`}
                />
                {formErrors.email && <div className='invalid-feedback'>{formErrors.email}</div>}
              </div>
              <div className='mb-3'>
                <label className='mb-2 text_font' htmlFor='password'>Password</label>
                <input
                  type="password"
                  id="password"
                  className={`form-control ${formErrors.password && 'is-invalid'} rounded-12`}
                />
                {formErrors.password && <div className='invalid-feedback'>{formErrors.password}</div>}
              </div>
              {apiError && <div className='alert alert-danger'>{apiError}</div>}
              <div className='mb-2 custom-font-size' style={{ color: "#4169E1" }}>
                <Link to="/forgotpassword" style={{textDecoration:"none"}}>Forgot password?</Link>
              </div>
              <div className="row">
                <p className="col-md-8 form-group mt-2 custom-font-size">Don't have an account?<Link to="/Signup"  style={{textDecoration:"none"}}>  Sign Up</Link></p>
                <div className="col-md-4 text-center">
                  <button className='signin_button mb-2' type="submit">SIGN IN</button>
                </div>
              </div>
            </form>
          </div>
        </div>
      </div>
    </div>
  );
}

export default Login;

