import React from 'react';
import { useState } from 'react';
import './herobanner.css';
import backgroundVideo from '../herobanner/attractions.mp4'; // Ensure the correct relative path to your video file
import Bubbles from './Bubbles';
import { Link } from 'react-router-dom';

const HeroBanner = () => {
    const token = localStorage.getItem('token');
    const [isLoggedIn] = useState(!!token);
    return (
        <>
            <div>
                <Bubbles />
            </div>
            <div className="hero-container">
                <video className="background-video img-fluid w-100" autoPlay loop muted>
                    <source src={backgroundVideo} type="video/mp4" />
                </video>
                <div className="hero-content">
                    <h1 className="custom-font">Unlock</h1>
                    <h1 className="custom-font">The Wonders</h1>
                    <p>“Your Gateway to Thrills and Excitement!”</p>
                    <Link to={isLoggedIn ? '/tickets' : '/signup'}>
                        <button className="button">BOOK NOW</button>
                    </Link>
                </div>
            </div>
        </>
    );
};

export default HeroBanner;
