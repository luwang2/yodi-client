import React, { useState } from 'react';
import { Carousel as BootstrapCarousel } from 'react-bootstrap';
import ExampleCarouselImage from '../Carousel/ExampleCarouselImg';
import "../Carousel/carousel.css";

function CustomCarousel() {
    const [activeIndex, setActiveIndex] = useState(0); // State to manage active index

    // Array of captions
    const captions = [
        "Escape the heat and embrace refreshing waters amidst stunning sunset vistas at our water park.",
        "Indulge in our bespoke Children's Zone, featuring personalized pools and slides designed exclusively for young adventurers at our water park.",
        "Lounging by the poolside where relaxation meets tranquility, sun kisses your skin and the pool cradles your dreams."
    ];

    // Function to handle slide selection
    const handleSelect = (selectedIndex, e) => {
        setActiveIndex(selectedIndex);
    };

    return (
        <div className="custom-carousel-container" >
            <BootstrapCarousel activeIndex={activeIndex} onSelect={handleSelect} interval={null}>
                <BootstrapCarousel.Item>
                    <ExampleCarouselImage text="First slide" />
                </BootstrapCarousel.Item>
                <BootstrapCarousel.Item>
                    <ExampleCarouselImage text="Second slide" />
                </BootstrapCarousel.Item>
                <BootstrapCarousel.Item>
                    <ExampleCarouselImage text="Third slide" />
                </BootstrapCarousel.Item>
            </BootstrapCarousel>
            <div className="carousel-caption-container">
                <p className="carousel-caption-item">{captions[activeIndex]}</p>
            </div>
        </div>
    );
}

export default CustomCarousel;
