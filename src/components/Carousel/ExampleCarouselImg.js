import React from 'react';
import '../Carousel/carousel.css';

const ExampleCarouselImage = ({ text }) => {
    // Example image paths, replace these with your actual image paths
    const imageSrc = {
        "First slide": '/Assets/0001.jpg',
        "Second slide": '/Assets/0002.jpg',
        "Third slide": '/Assets/0003.jpg'
    };

    return (
        <div className="carousel-image-container" >
            <img
                className="d-block w-50 custom-image-height"
                style={{ boxShadow: '3px 3px 5px rgba(0, 0, 0, 0.2)' }}
                src={imageSrc[text] || "/assets/images/default-slide.jpg"} // Fallback to a default image if text doesn't match
                alt={text}
            />
        </div>
    );
};

export default ExampleCarouselImage;
