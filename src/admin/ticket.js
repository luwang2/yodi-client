import React, { useEffect, useState } from "react";
import axios from "axios";
import 'bootstrap/dist/css/bootstrap.min.css';
import 'bootstrap/dist/js/bootstrap.bundle.min.js';
import { Modal, Form } from "react-bootstrap";
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

export default function Tickets() {
    const [tickets, setTickets] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [value, setValue] = useState({
        name: '',
        age: '',
        price: ''
    });

    const fetchTickets = async () => {
        try {
            const response = await axios.get("http://localhost:8080/api/tickets");
            if (response.data.success) {
                setTickets(response.data.tickets);
            }
        } catch (error) {
            console.error(error);
        }
    };

    useEffect(() => {
        fetchTickets();
    }, []);

    const handleChange = (e) => {
        setValue({
            ...value,
            [e.target.name]: e.target.value
        });
    };

    const handleAddTicket = async (e) => {
        e.preventDefault();
        try {
            const response = await axios.post('http://localhost:8080/api/tickets', value);
            if (response.data.success) {
                toast.success("Ticket has been added!");
                setValue({ name: '', age: '', price: '' });
                fetchTickets();
                setShowModal(false);
            }
        } catch (error) {
            console.error(error);
        }
    };

    const handleDeleteTicket = async (id) => {
        try {
            const response = await axios.delete(`http://localhost:8080/api/tickets/${id}`);
            if (response.data.success) {
                toast.success("Ticket has been deleted!");
                fetchTickets();
            }
        } catch (error) {
            console.error(error);
        }
    };

    return (
        <>
            <div className="row">
                <div className="col-sm-6"></div>
                <div className="col-sm-6 text-end">
                    <button
                        className="btn btn-icon pe-5 ps-5 mb-5"
                        onClick={() => setShowModal(true)}
                        style={{ backgroundColor: 'transparent', border: 'none', color: 'inherit' }}
                    >
                        <i className="fas fa-plus"></i> Add Ticket
                    </button>
                </div>
            </div>
            <div className="ticket_page container-fluid d-flex row" style={{ width: "100%" }}>
                <div className="row justify-content-center">
                    {tickets.map((ticket, index) => (
                        <div className="my-5" key={index}>
                            <div className="col-12 col-md-12 my-5" key={index}>
                                <div className="bg-white border rounded  mx-auto ticket-container">
                                    <div className="blue-line mb-3"></div>
                                    <div className="d-flex align-items-center">
                                        <div className="flex-grow-1">
                                            <p>Name: {ticket.name}</p>
                                            <p>Age: {ticket.age}</p>
                                            <p>Price: {ticket.price}</p>
                                        </div>
                                        <div className='me-3'>
                                            <img className='img-fluid ticket-logo me-4' src='../Assets/logoBlue.png' alt='logo'></img>
                                        </div>
                                        <div className="ticket_bar"></div>
                                    </div>
                                </div>
                            </div>
                            <div className="del_buttons row">
                                <button
                                    className="btn"
                                    onClick={() => handleDeleteTicket(ticket._id)}
                                >
                                    <i className="fas fa-trash-alt"></i> Remove
                                </button>
                            </div>
                        </div>
                    ))}
                </div>
            </div>

            <Modal className="center-modal" show={showModal} onHide={() => setShowModal(false)}>
                <Form noValidate onSubmit={handleAddTicket}>
                    <Modal.Header closeButton>
                        <Modal.Title>Add Ticket</Modal.Title>
                    </Modal.Header>
                    <Modal.Body>
                        <Form.Group className="mb-3" controlId="formName">
                            <Form.Label>Name</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter name"
                                name="name"
                                value={value.name}
                                onChange={handleChange}
                                required
                            />
                            <Form.Control.Feedback type="invalid">
                                Please provide a name.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formAge">
                            <Form.Label>Age</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter age"
                                name="age"
                                value={value.age}
                                onChange={handleChange}
                                required
                            />
                            <Form.Control.Feedback type="invalid">
                                Please provide an age.
                            </Form.Control.Feedback>
                        </Form.Group>
                        <Form.Group className="mb-3" controlId="formPrice">
                            <Form.Label>Price</Form.Label>
                            <Form.Control
                                type="text"
                                placeholder="Enter price"
                                name="price"
                                value={value.price}
                                onChange={handleChange}
                                required
                            />
                            <Form.Control.Feedback type="invalid">
                                Please provide a price.
                            </Form.Control.Feedback>
                        </Form.Group>
                    </Modal.Body>
                    <Modal.Footer>
                        <button className="custom-btn" onClick={() => setShowModal(false)}>
                            Cancel
                        </button>
                        <button type="submit" className="upload-button px-5 py-2 ms-4 border-radius">
                            Add
                        </button>
                    </Modal.Footer>
                </Form>
            </Modal>

            <ToastContainer />
        </>
    );
}
