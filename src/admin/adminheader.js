// import React, { useState } from 'react';
// import '../css/admin/header.css';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import { NavLink } from 'react-router-dom';
// import { Modal } from 'react-bootstrap';


// const Navbar = () => {
//     const [isMenuOpen, setIsMenuOpen] = useState(false);
//     const [showOverlay, setShowOverlay] = useState(false);

//     const toggleMenu = () => {
//         setIsMenuOpen(!isMenuOpen);
//     };

//     const handleLogoutClick = () => {
//         setShowOverlay(true);
//     };
//     const handleClose = () => {
//         setShowOverlay(false);
//     };
//     function logout() {
//         fetch('http://localhost:8080/api/user/logout')
//             .then(response => {
//                 if (response.ok) {
//                     localStorage.removeItem('token');
//                     localStorage.removeItem('isAdmin');
//                     window.open("./", "_self")
//                 } else {
//                     throw new Error(response.statusText)
//                 }
//             }).catch(e => {
//                 alert(e)
//             })
//     }

//     return (
//         <div className='d-flex row bg-white '>
//             <header className="py-3">
//                 <div className="container d-flex w-100 justify-content-center">
//                     <NavLink to="/admin/home" className="d-flex align-items-center text-dark text-decoration-none">
//                         <img className='adminLogo' src="../../.././Assets/logoblue.png" alt="logo" />
//                     </NavLink>
//                 </div>
//             </header>
//             <nav className="admin_navbar  navbar-expand-sm navbar-dark col-12 mb-2 justify-content-center ">
//                 <div className="container d-flex">
//                     <div className='d-flex '>
//                         <button className="navbar-toggler" type="button" onClick={toggleMenu}>
//                             <span className="navbar-toggler-icon"></span>
//                         </button>
//                     </div>
//                     <div className={`collapse text-center navbar-collapse ${isMenuOpen ? 'show' : ''}`}>
//                         <div className='d-flex w-100 justify-content-center'>
//                             <ul className="navbar-nav">
//                                 <li className="navbar-item">
//                                     <NavLink to="/admin/home" className="nav-link px-4 text-white">Home</NavLink>
//                                 </li>
//                                 <li className="navbar-item">
//                                     <NavLink to="/admin/transactions" className="nav-link px-4 text-white">Transactions</NavLink>
//                                 </li>
//                                 <li className="navbar-item">
//                                     <NavLink to="/admin/aboutus" className="nav-link px-4 text-white">About</NavLink>
//                                 </li>

//                                 <li className="navbar-item">
//                                     <NavLink to="/admin/attraction" className="nav-link px-4 text-white">Attraction</NavLink>
//                                 </li>
//                                 <li className="navbar-item">
//                                     <NavLink to="/admin/tickets" className="nav-link px-4 text-white">Tickets</NavLink>
//                                 </li>
//                                 <li className="navbar-item">
//                                     <NavLink
//                                         className="nav-link px-4 text-white"
//                                         onClick={handleLogoutClick} style={{ textDecoration: "none", color: "inherit" }}
//                                     >
//                                         Logout
//                                     </NavLink>
//                                 </li>
//                             </ul>
//                         </div>
//                     </div>
//                 </div>
//             </nav >



//             <Modal className="center-modal p-5 text-center" show={showOverlay} onHide={handleClose}>
//                 <Modal.Header closeButton>
//                     <Modal.Title className='w-100 text-center'>Are you sure you want to log out?</Modal.Title>
//                 </Modal.Header>
//                 <Modal.Body>
//                     You will be signed out of your account and any unsaved changes may be lost.
//                 </Modal.Body>
//                 <Modal.Footer className="d-flex row justify-content-around">
//                     <button
//                         className="logout-custom-btn"
//                         onClick={() => setShowOverlay(false)}
//                         style={{ width: '120px', height: '40px', fontSize: '14px' }}
//                     >
//                         Cancel
//                     </button>
//                     <button
//                         className="logout-button"
//                         onClick={() => logout()}
//                         style={{ width: '120px', height: '40px', fontSize: '14px' }}
//                     >
//                         Log Out
//                     </button>
//                 </Modal.Footer>
//             </Modal>
//         </div >
//     );
// };

// export default Navbar;


import React, { useState } from 'react';
import '../css/admin/header.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import { NavLink } from 'react-router-dom';
import { Modal } from 'react-bootstrap';

const Navbar = () => {
    const [isMenuOpen, setIsMenuOpen] = useState(false);
    const [showOverlay, setShowOverlay] = useState(false);

    const toggleMenu = () => {
        setIsMenuOpen(!isMenuOpen);
    };

    const handleLogoutClick = () => {
        setShowOverlay(true);
    };
    const handleClose = () => {
        setShowOverlay(false);
    };
    function logout() {
        fetch('http:localhost:8080/api/user/logout')
            .then(response => {
                if (response.ok) {
                    localStorage.removeItem('token');
                    localStorage.removeItem('isAdmin');
                    window.open("./", "_self")
                } else {
                    throw new Error(response.statusText)
                }
            }).catch(e => {
                alert(e)
            })
    }

    return (
        <div className='d-flex row bg-white '>
            <header className="py-3">
                <div className="container d-flex w-100 justify-content-center">
                    <NavLink to="/admin/home" className="d-flex align-items-center text-dark text-decoration-none">
                        <img className='adminLogo' src="../../.././Assets/logoblue.png" alt="logo" />
                    </NavLink>
                </div>
            </header>
            <nav className="admin_navbar  navbar-expand-sm navbar-dark col-12 mb-2 justify-content-center ">
                <div className="container d-flex">
                    <div className='d-flex '>
                        <button className="navbar-toggler" type="button" onClick={toggleMenu}>
                            <span className="navbar-toggler-icon"></span>
                        </button>
                    </div>
                    <div className={`collapse text-center navbar-collapse ${isMenuOpen ? 'show' : ''}`}>
                        <div className='d-flex w-100 justify-content-center'>
                            <ul className="navbar-nav">
                                <li className="navbar-item">
                                    <NavLink to="/admin/home" className="nav-link px-4 text-white" activeClassName="active">Home</NavLink>
                                </li>
                                <li className="navbar-item">
                                    <NavLink to="/admin/transactions" className="nav-link px-4 text-white" activeClassName="active">Transactions</NavLink>
                                </li>
                                <li className="navbar-item">
                                    <NavLink to="/admin/aboutus" className="nav-link px-4 text-white" activeClassName="active">About</NavLink>
                                </li>
                                <li className="navbar-item">
                                    <NavLink to="/admin/attraction" className="nav-link px-4 text-white" activeClassName="active">Attraction</NavLink>
                                </li>
                                <li className="navbar-item">
                                    <NavLink to="/admin/tickets" className="nav-link px-4 text-white" activeClassName="active">Tickets</NavLink>
                                </li>
                                <li className="navbar-item">
                                    <NavLink
                                        className="nav-link px-4 text-white"
                                        onClick={handleLogoutClick}
                                        style={{ textDecoration: "none", color: "inherit" }}
                                        activeClassName="active"
                                    >
                                        Logout
                                    </NavLink>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </nav >

            <Modal className="center-modal p-5 text-center" show={showOverlay} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title className='w-100 text-center'>Are you sure you want to log out?</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    You will be signed out of your account and any unsaved changes may be lost.
                </Modal.Body>
                <Modal.Footer className="d-flex row justify-content-around">
                    <button
                        className="logout-custom-btn"
                        onClick={() => setShowOverlay(false)}
                        style={{ width: '120px', height: '40px', fontSize: '14px' }}
                    >
                        Cancel
                    </button>
                    <button
                        className="logout-button"
                        onClick={() => logout()}
                        style={{ width: '120px', height: '40px', fontSize: '14px' }}
                    >
                        Log Out
                    </button>
                </Modal.Footer>
            </Modal>
        </div >
    );
};

export default Navbar;
